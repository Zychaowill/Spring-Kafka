package com.project.mq.schedule;

import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import com.project.mq.bo.Message;

@Component
@EnableScheduling
public class KafkaProducerScheduler {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Scheduled(fixedRate = 3 * 1000)
	public void send() {
		Integer number = new Random().nextInt(100);
		String message = UUID.randomUUID().toString();
		Message msg = new Message(number, message);
		ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send("test", "chat", msg.toString());
		future.addCallback(o -> System.out.println("Send message successfully! Message is " + msg.toString()),
				throwable -> System.out.println("Send message error! Message is " + msg.toString()));
	}
}
