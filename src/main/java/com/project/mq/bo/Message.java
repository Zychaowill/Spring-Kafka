package com.project.mq.bo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Message implements Serializable {

	private static final long serialVersionUID = -224095133239442016L;
	private Integer error;
	private String message;

	@Override
	public String toString() {
		return "{\"error\":\"" + error + "\", \"message\":\"" + message + "\"}";
	}

}
