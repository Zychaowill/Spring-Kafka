package com.project.mq.bo.response;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Response {
	ResultCode code;
	String message;
	Object o;
}
