package com.project.mq.configuration;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Listener {

	@KafkaListener(topics = { "test" })
	public void listen(ConsumerRecord<?, ?> record) {
		log.info("The key of kafka is {}.", record.key());
		log.info("The value of kafka is {}.", record.value().toString());
	}
}
