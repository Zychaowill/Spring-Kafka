package com.project.mq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.mq.bo.response.Response;
import com.project.mq.bo.response.ResultCode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/kafka")
public class CollectController {
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@RequestMapping(value = "/send", method = RequestMethod.GET)
	public Response sendKafka(@RequestParam("message") String message) {
		try {
			log.info("The message that send to Kafka is {}.", message);
	        kafkaTemplate.send("test", "chat", message);
	        log.info("Send Message Successfully.");
	        return new Response(ResultCode.SUCCESS, "Send Message Successfully!", null);
		} catch (Exception e) {
			log.error("Send Error!", e);
			return new Response(ResultCode.EXCEPTION, "Send Message Error!", null);
		}
	}
}
