# Spring-Kafka

Spring integrate with Kafka. This project based on Spring Boot 2.0.

- run zookeeper

```bash
./bin/zkServer.sh
```

- run kafka server

```bash
./bin/kafka-server-start.sh ./config/server.properties
```

- create kafka topic

```bash
./bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partition 1 --topic test
```

- run kafka console producer

```bash
./bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
```

- run kafka console consumer

```bash
./bin/kafka-console-consumer.sh --zookeeper localhost:2181 --topic test
```

### Kafka常用命令

- 列出主题

```bash
./bin/kafka-topic.sh -list --zookeeper localhost:2181
```

- 描述主题

```bash
./bin/kafka-topic.sh -describe --zookeeper localhost:2181 -topic [topic name]
```

- 从头读取消息

```bash
./bin/kafka-console-consumer.sh --zookeeper localhost:2181 -topic [topic name] -from-beginning
```

- 删除主题

```bash
./bin/kafka-run-class.sh kafka.admin.TopicCommand -delete -topic [topic_to_delete] -zookeeper localhost:2181
```

- 查看topic的详细信息

```bash
./bin/kafka-topic.sh --zookeeper localhost:2181 -describe -topic [topic name]
```

- 为topic增加副本

```bash
./bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 --reassignment-json-file json/partitions-to-move.json -execute
```

- 为topic添加partition

```bash
./bin/kafka-topic.sh --zookeeper localhost:2181 -alter -partitions 20 -topic [topic name]
```

- 下线broker

```bash
./bin/kafka-run-class.sh kafka.admin.ShutdownBroker --zookeeper localhost:2181 broker [brokerId] --num.retries 3 -retry.interval.ms 60 shudown broker
```